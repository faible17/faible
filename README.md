# README #

This is the fAIble dynamic storytelling system, developed over Summer 2017 in Germany as part of the National Science Foundation's International Research Experience for Students program.

### Contribution guidelines ###

* Please make an issue for each task you would like to work on.
* When working on a feature or task, make a new branch. Branches should correspond to an issue (with the exception of very small tasks that can be done in existing branches).
* Keep features contained in branches before merging to the master copy. All code in branches should be thoroughly tested and debugged before committing to master.

### Who do I talk to? ###

* Team lead: Vera Kazakova
* Git-related issues: Lauren Hastings
* Storytelling algorithm: Vera Kazakova & Lauren Hastings
* NLG: Andres Posadas & Lucas Gonzalez