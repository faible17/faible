 import simplenlg.framework.*;
 import simplenlg.lexicon.*;
 import simplenlg.realiser.english.*;
 import simplenlg.phrasespec.*;
 import simplenlg.features.*;
 import java.util.*;
 
class Character
{
	String name;
	String type;
	ArrayList<String> descriptors;
}

class Object
{
	String name;
	String type;
	ArrayList<String> descriptors;
}

class Action
{
	String verb;
	ArrayList<String> adverbs;
}

class Location
{
	String name;
	ArrayList<String> descriptors;
}
 
public class AlphaModule {
	Lexicon lexicon = Lexicon.getDefaultLexicon();
	NLGFactory factory = new NLGFactory(lexicon);
	Realiser realiser = new Realiser();
	
	int count = 0;
	
	ArrayList<Character> characters = new ArrayList<Character>();
	ArrayList<Object> objects = new ArrayList<Object>();
	ArrayList<Action> actions = new ArrayList<Action>();
	
	// Prepositional phrases will be added as part of the adverbs/descriptors of the action. 
	String PrepositionalSentence()
	{
		SPhraseSpec builder = new SPhraseSpec(factory);
		VPPhraseSpec verb = factory.createVerbPhrase();
		PPPhraseSpec preposition = factory.createPrepositionPhrase();
		
		builder.setSubject(characters.get(count).name);
		
		verb.setVerb(actions.get(count).verb);
		verb.addComplement(actions.get(count).adverbs.get(0));
		builder.setVerb(verb);
		
		preposition.setObject(objects.get(count).name);
		preposition.setPreposition(objects.get(count).descriptors.get(0));
		builder.setObject(preposition);	
		
		// Need to restructure this so that tense comes from the action that is passed in.
		builder.setFeature(Feature.TENSE, Tense.PAST);
		
		return realiser.realiseSentence(builder);
	}
	
	String SimpleSentence()
	{
		SPhraseSpec builder = new SPhraseSpec(factory);
		NPPhraseSpec noun = factory.createNounPhrase();
		
		noun.setNoun(objects.get(count).name);
		noun.setDeterminer("the");
		
		builder.setSubject(characters.get(count).name);
		builder.setVerb(actions.get(count).verb);
		builder.setObject(noun);
		
		builder.setFeature(Feature.TENSE, Tense.PRESENT);
		
		return realiser.realiseSentence(builder);
	}
	
	String DescriptiveSentence()
	{
		SPhraseSpec builder = new SPhraseSpec(factory);
		// Implement determiner logic.
		NPPhraseSpec noun = factory.createNounPhrase(characters.get(count).name);
		VPPhraseSpec verb = factory.createVerbPhrase(actions.get(count).verb);
		
		noun.setDeterminer("the");
		verb.addComplement(actions.get(count).adverbs.get(0));
		
		builder.setSubject(noun);
		builder.setVerb(verb);
		
		return realiser.realiseSentence(builder);		
	}
	
	public static void main(String[] args) {
		AlphaModule generator = new AlphaModule();
		
		Character mike = new Character();
		
		mike.name = "Mike";
		mike.type = "nerd";
		mike.descriptors = new ArrayList<String>();
		mike.descriptors.add("Nerdy");
		
		Action laughed = new Action();
		
		laughed.verb = "laugh";
		laughed.adverbs = new ArrayList<String>();
		
		laughed.adverbs.add("shyly");
		
		generator.characters.add(mike);
		generator.actions.add(laughed);
		
		System.out.println(generator.DescriptiveSentence());
	}
}
